@extends('layouts/master')
@section('content')
<h1 class="h3 mb-4 text-gray-800">{{$title}}</h1>
<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">Show detail game</h3>
    </div>
    <div class="card-body box-profile">
        <h3 class="profile-username">{{$game->name}} ({{$game->year}})</h3>

        <p class="text-muted">Developer: {{$game->developer}}</p>

        <p><strong>Gameplay:</strong><br>{{$game->gameplay}}</p>

        <p>
            <strong>Platform:</strong><br>
            @foreach ($platforms as $key=>$value)
            <span style="background-color:purple; color:white; border-radius:10px;" class="p-1">{{$value->name}}</span>
            @endforeach
        </p>
        <a href="{{url('game')}}" class="btn btn-primary btn-block"><b>Back</b></a>
    </div>

    @endsection