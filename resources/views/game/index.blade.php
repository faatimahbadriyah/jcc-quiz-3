@extends('layouts/master')
@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800">{{$title}}</h1>
<div class="card card-primary card-outline">
    <div class="card-header">
        <a href="{{url('game/create')}}" class="btn btn-primary">Add New Game</a>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table id="tb_games" class="table table-hover text-nowrap">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Developer</th>
                    <th>Year</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($games as $key=>$value)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$value->name}}</td>
                    <td>{{$value->developer}}</td>
                    <td>{{$value->year}}</td>
                    <td>
                        <a href="/game/{{$value->id}}" class="btn btn-success btn-sm btn-block">Show</a>
                        <a href="/game/{{$value->id}}/edit" class="btn btn-warning btn-sm btn-block">Edit</a>
                        <form action="/game/{{$value->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger btn-sm btn-block mt-2" value="Delete">
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->

</div>
@endsection

@push('scripts')
<script src="{{asset('sbadmin2/vendor/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('sbadmin2/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('sbadmin2/js/swal.min.js')}}"></script>
<script>
$(function() {
    $("#tb_games").DataTable();
});
Swal.fire({
    title: "Berhasil!",
    text: "Memasangkan script sweet alert",
    icon: "success",
    confirmButtonText: "Cool",
});
</script>
@endpush