@extends('layouts/master')
@section('content')
<h1 class="h3 mb-4 text-gray-800">{{$title}}</h1>
<div class="card card-primary card-outline">
    <div class="card-header">
        <h3 class="card-title">Add new game</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="{{url('/game')}}" method="POST">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" name="name" class="form-control" id="name" required>
            </div>
            <div class="form-group">
                <label for="gameplay">Gameplay</label>
                <textarea name="gameplay" class="form-control" id="gameplay" required></textarea>
            </div>
            <div class="form-group">
                <label for="developer">Developer</label>
                <input type="text" name="developer" class="form-control" id="developer" required>
            </div>
            <div class="form-group">
                <label for="year">Year</label>
                <input type="number" name="year" class="form-control" id="year" required>
            </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="{{url('game')}}" class="btn btn-default float-right">Cancel</a>
        </div>
    </form>
</div>

@endsection