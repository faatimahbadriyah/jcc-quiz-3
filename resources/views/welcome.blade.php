@extends('layouts/master')

@section('content')
<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800">Entity Relationship Diagram</h1>
<img class="img-fluid" src="{{asset('ERD.jpeg')}}">
@endsection