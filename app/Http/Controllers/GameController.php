<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;

class GameController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $games = DB::table('game')->get();
        $data = [
            'title' => 'GAME LIST',
            'games' => $games,
        ];
        return view('game.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'title' => 'GAME CREATE',
        ];
        return view('game.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $query = DB::table('game')->insert([
            "name" => $request["name"],
            "gameplay" => $request["gameplay"],
            "developer" => $request["developer"],
            "year" => $request["year"],
        ]);
        return redirect('/game');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $game = DB::table('game')->where('id', $id)->first();
        $platforms = DB::table('platform')
            ->select('name')
            ->where('game_id', $id)
            ->get();
        $data = [
            'title' => 'GAME SHOW',
            'game' => $game,
            'platforms' => $platforms,
        ];
        return view('game.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $game = DB::table('game')->where('id', $id)->first();
        $data = [
            'title' => 'GAME EDIT',
            'game' => $game,
        ];
        return view('game.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $game = DB::table('game')
            ->where('id', $id)
            ->update([
                "name" => $request["name"],
                "gameplay" => $request["gameplay"],
                "developer" => $request["developer"],
                "year" => $request["year"],
            ]);
        return redirect('/game');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $query = DB::table('game')->where('id', $id)->delete();
        return redirect('game');
    }
}